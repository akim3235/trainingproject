public class Practice{

    public static void main(String[] args) {
        String car = "Mercedes best german car";
        String car2 = car.replace("Mercedes","BMW");
        System.out.println(car2);


        String str = "I am a string";
        System.out.println(str.length());

        String string1 = "cat";
        String string2 = "CAT";
        System.out.println(string1.equalsIgnoreCase(string2));

        int month = 9;
        if (month == 1) {
            System.out.println("January");
        } else if (month == 2) {
            System.out.println("February");
        } else if (month == 3) {
            System.out.println("March");
        } else if (month == 4) {
            System.out.println("April");
        } else if (month == 5) {
            System.out.println("May");
        } else if (month == 6) {
            System.out.println("June");
        } else if (month == 7) {
            System.out.println("July");
        } else if (month == 8) {
            System.out.println("August");
        } else if (month == 9) {
            System.out.println("September");
        } else if (month == 10) {
            System.out.println("October");
        } else if (month == 11) {
            System.out.println("November");
        } else if (month == 12) {
            System.out.println("December");
        } else {
            System.out.println("not a month");
        }

        String sentence = "The bootcamp is helpful, but challenging";
        int length = sentence.length();
        if (length%2 == 0) {
            System.out.println("The string is even");
        } else {
            System.out.println("The string is not even");
        }
// to be continued
    }
}