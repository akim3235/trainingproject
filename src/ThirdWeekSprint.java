public class ThirdWeekSprint {

    public static void main (String[] args) {

        recursionFizzBuzz(1);

        /*for (int y = 1; y < 101; y++) {
            if (y % 15 == 0) {
                System.out.println("FizzBuzz");
            }
            else if (y % 5 == 0) {
                System.out.println("Buzz");
            }
            else if (y % 3 == 0) {
                System.out.println("Fizz");
            }
            else {
                System.out.println(y);
            }
        }*/
    }

    public static void recursionFizzBuzz(int n) {
        if (n >= 101) {
            return;
        }
        if (n % 15 == 0) {
            System.out.println("FizzBuzz");
        }
        else if (n % 5 == 0) {
            System.out.println("Buzz");
        }
        else if (n % 3 == 0) {
            System.out.println("Fizz");
        }
        else {
            System.out.println(n);
        }
        recursionFizzBuzz(n + 1);

    }
}
