public class JiraTasks {

    public static void main(String[] args) {

        System.out.println("Alex Kim");

        System.out.println(45 + (float)78 / 87);

        int A = 8;
        String str1 = Integer.toString(A);

        String myName = "Alex Kim";
        System.out.println("My name is " + myName);

        int someInt = 9;
        if (someInt%2 != 0) {
            System.out.println("number is Odd");
        }


        //Switch syntax
        char penaltyKick = 'C';

        switch (penaltyKick) {
            case 'L': System.out.println("Messi shoots to the left and scores!");
                break;
            case 'R': System.out.println("Messi shoots to the right and misses the goal!");
                break;
            case 'C': System.out.println("Messi shoots down the center, but the keeper blocks it!");
                break;
            default:
                System.out.println("Messi is in position...");
        }
    }
}
